package com.tarek.animatediconbutton

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import com.tarek.animatediconbutton.animation.MyBounceInterpolator

class AnimatedIconButton(context: Context, attrs: AttributeSet) : ImageButton(context, attrs) {

    var iconState = NOT_FAV
        set(state) {
            field = state
            invalidate()
        }

    var notFavRes: Int = R.drawable.ic_favorite_border_white_24dp
        set(value) {
            field = value
            invalidate()
        }

    var favRes: Int = R.drawable.ic_favorite_red_24dp
        set(value) {
            field = value
            invalidate()
        }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (iconState == NOT_FAV)
            setBackgroundResource(notFavRes)
        else
            setBackgroundResource(favRes)
    }


    init {
        setupAttributes(attrs)
        setBackgroundColor(Color.TRANSPARENT)

        if (iconState == NOT_FAV)
            setBackgroundResource(notFavRes)
        else
            setBackgroundResource(favRes)

    }

    companion object {
        const val NOT_FAV = 0L
        const val FAV = 1L
    }

    private fun setupAttributes(attrs: AttributeSet?) {
        // Obtain a typed array of attributes
        val typedArray = context.theme.obtainStyledAttributes(
            attrs, R.styleable.AnimatedIconButton,
            0, 0
        )

        // Extract custom attributes into member variables
        iconState =
            typedArray.getInt(R.styleable.AnimatedIconButton_state, NOT_FAV.toInt()).toLong()

        typedArray.recycle()
    }

    override fun onSaveInstanceState(): Parcelable {
        val bundle = Bundle()

        bundle.putLong("iconState", iconState)

        bundle.putParcelable("superState", super.onSaveInstanceState())
        return bundle
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        var viewState = state

        if (viewState is Bundle) {
            iconState = viewState.getLong("iconState", NOT_FAV)

            viewState = viewState.getParcelable("superState")!!
        }
        super.onRestoreInstanceState(viewState)
    }

    private fun animateIcon() {
        val myAnim: Animation = AnimationUtils.loadAnimation(context, R.anim.bounce)

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        val interpolator = MyBounceInterpolator(0.2, 20.0)
        myAnim.interpolator = interpolator

        startAnimation(myAnim)
    }


    fun onClicked(myFunc: () -> Unit) {
        setOnClickListener {
            if (iconState == NOT_FAV)
                animateIcon()

            myFunc()

            if (iconState == NOT_FAV)
                iconState = FAV
            else
                iconState = NOT_FAV
        }
    }

}